﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows;
using System.Windows.Input;

namespace Domineering
{
    public class Bord
    {
        private int x;
        private int y;
        private int breedteBlok;
        private int hoogteBlok;
        private int xPos;
        private int yPos;

        public Bord(int rasterGrootte, Canvas canvas)
        {
            breedteBlok = (int)canvas.Width / rasterGrootte;
            hoogteBlok = (int)canvas.Height / rasterGrootte;
            DrawRaster(rasterGrootte, canvas);
        }

        public void DrawRaster(int rasterGrootte, Canvas canvas)
        {
            for (int i = 0; i < rasterGrootte; i++)
            {
                for (int j = 0; j < rasterGrootte; j++)
                {
                    
                    Rectangle rect = new Rectangle();
                    rect.Width = breedteBlok;
                    rect.Height = hoogteBlok;
                    rect.Stroke = new SolidColorBrush(Colors.Black);
                    rect.Margin = new Thickness(x, y, 0, 0);
                    canvas.Children.Add(rect);
                    x += breedteBlok;
                 }
                x = 0;
                y += hoogteBlok;
            }
           
           }

        public string PositieMuisOpRaster(Canvas canvas)
        {
            xPos = Convert.ToInt32(Mouse.GetPosition(canvas).X) / breedteBlok;
            yPos = Convert.ToInt32(Mouse.GetPosition(canvas).Y) / hoogteBlok;

            String positie = "(" + (yPos + 1) + "," + (xPos + 1) + ")";
            return positie;
        }

        public int GetBreedte
        {
            get
            {
                return breedteBlok;
            }
        }

        public int GetHoogte
        {
            get
            {
                return hoogteBlok;
            }
        }

        public int XCoord
        {
            get
            {
                return xPos;
            }
        }

        public int YCoord
        {
            get
            {
                return yPos;
            }
        }


    }
}
