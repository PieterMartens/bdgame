﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Domineering
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Bord bord;
        Block blok;
        int beurtSpelerTel = 0;

        public MainWindow()
        {
            InitializeComponent();
            bord = new Bord(8, rasterCanvas);
        }

        private void RasterCanvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            positieLabel.Content = bord.PositieMuisOpRaster(rasterCanvas);
            xLabel.Content = "X Coordinate " + Mouse.GetPosition(rasterCanvas).X;
            yLabel.Content = "Y Coordinate " + Mouse.GetPosition(rasterCanvas).Y;

            beurtSpelerTel++;
            if (beurtSpelerTel % 2 == 1)
            {
                blok = new Block(bord.XCoord * bord.GetBreedte, bord.YCoord * bord.GetHoogte, bord.GetBreedte, bord.GetHoogte, rasterCanvas, Colors.Blue);
                blok.VerticalBlock(bord.XCoord * bord.GetBreedte, bord.YCoord * bord.GetHoogte, bord.GetBreedte, bord.GetHoogte, rasterCanvas, Colors.Blue);
                
            } else
            {
                blok = new Block(bord.XCoord * bord.GetBreedte, bord.YCoord * bord.GetHoogte, bord.GetBreedte, bord.GetHoogte, rasterCanvas, Colors.Red);
                blok.HorizontalBlock(bord.XCoord * bord.GetBreedte, bord.YCoord * bord.GetHoogte, bord.GetBreedte, bord.GetHoogte, rasterCanvas, Colors.Red);
            }

            
           

        }
    }
}
