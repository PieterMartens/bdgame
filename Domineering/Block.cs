﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Domineering
{
    public class Block
    {
        

        public Block(int x, int y, int breedte, int grootte, Canvas canvas, Color kleur)
        {
            DrawBlock(x, y, breedte, grootte,canvas,kleur);
        }

        private void DrawBlock(int x, int y, int breedte, int grootte, Canvas canvas, Color kleur)
        {
            Rectangle rect = new Rectangle();
            rect.Width = breedte;
            rect.Height = grootte;
            rect.Margin = new Thickness(x, y, 0, 0);
            rect.Fill = new SolidColorBrush(kleur);
            canvas.Children.Add(rect);
        }

        public void VerticalBlock(int x, int y, int breedte, int grootte, Canvas canvas, Color kleur)
        {
            DrawBlock(x, y, breedte, grootte * 2, canvas, kleur);
        }

        public void HorizontalBlock(int x, int y, int breedte, int grootte, Canvas canvas, Color kleur)
        {
            DrawBlock(x , y, breedte * 2, grootte, canvas, kleur);
        }

        
    }
}
